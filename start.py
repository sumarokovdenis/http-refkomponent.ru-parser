import time

from bs4 import BeautifulSoup
from requests import Session
import os
import shutil
from openpyxl import Workbook
from openpyxl.styles import Border
from openpyxl.styles import Side, Alignment
from selenium import webdriver

SITE_URL = 'http://refkomponent.ru'
user_agent = ("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 "
              "(KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36")

login_url = 'http://refkomponent.ru'

class Parser:

    def __init__(self):
        # self.driver = webdriver.Firefox()

        self.session = Session()
        self.catalog_dict = {}
        self.cards_dict = {}

        self.session.headers.update({  # Притворимся браузером
            'User-agent': 'Mozilla/5.0 (Windows NT 6.1; rv:40.0) '
                          'Gecko/20100101 Firefox/40.0'
        })

        self.wb = Workbook()
        self.ws = self.wb.active

    def start_parse(self):
        self.session.get(SITE_URL)
        response = self.session.get(SITE_URL + '/catalog/')
        bs = BeautifulSoup(response.text)
        div_content = bs.find('div', attrs={'id': 'content'})

        as_contnet = div_content.findAll('a')

        # collect catalog links and texts

        for a in as_contnet:

            a_src = a.attrs.get('href')
            a_text = a.text
            self.catalog_dict.update({a_src: a_text})

        # find cards in catalog page

        side = Side(style='medium')
        border = Border(left=side, right=side,
                        top=side, bottom=side)
        column = 1
        row = 1

        for catalog_src in self.catalog_dict:
            try:
                display_items = []

                catalog_full_url = SITE_URL + catalog_src
                req = self.session.get(catalog_full_url)

                bs_c = BeautifulSoup(req.text)

                display_list = bs_c.findAll('div', {'class': 'list_item_wrapp'})

                for item in display_list:
                    display_items.append(item)

                module_paginator = bs_c.find('div', {'class': 'module-pagination'})

                paginator_links = []

                if module_paginator:
                    paginator_span = module_paginator.find('span')
                    all_as = paginator_span.findAll('a')
                    for a in all_as:
                        href = a.attrs.get('href')
                        if href and not href == '#' and not (href in paginator_links):
                            paginator_links.append(a.attrs.get('href'))

                for paginator_link in paginator_links:
                    new_req = self.session.get(SITE_URL + paginator_link)

                    bs_c = BeautifulSoup(new_req.text)

                    display_list = bs_c.findAll('div', {'class': 'list_item_wrapp'})

                    for item in display_list:
                        display_items.append(item)

                for item in display_items:
                    row_list = []

                    description_wrapp_td = item.find('td', {'class': 'description_wrapp'})
                    desc_name = description_wrapp_td.find('div', {'class': 'desc_name'})
                    name_span = desc_name.find('span')

                    if name_span:
                        name_text = name_span.text
                        row_list.append(name_text)
                        print('# start # ', name_text)

                    preview_text_div = description_wrapp_td.find('div', {'class': 'preview_text'})
                    if preview_text_div:
                        preview_text = preview_text_div.text
                        row_list.append(preview_text)

                    information_wrapp_td = item.find('td', {'class': 'information_wrapp'})
                    div_label = information_wrapp_td.find('div', {'class': 'label'})
                    span_value = div_label.find('span', {'class': 'value'})
                    if span_value:
                        avail_text = span_value.text
                        row_list.append(avail_text)

                    div_price = information_wrapp_td.find('div', {'class': 'price'})
                    price_text = 'unknown'
                    if div_price:
                        price_text = div_price.text
                    row_list.append(price_text)

                    props_list_wrapp_div = description_wrapp_td.find('div', {'class': 'props_list_wrapp'})
                    if props_list_wrapp_div:
                        props_list_table = props_list_wrapp_div.find('table')
                        trs = props_list_table.findAll('tr')
                        for tr in trs:
                            tds = tr.findAll('td')
                            for td in tds:
                                props_name_text = td.text
                                text = props_name_text.replace('\n', '')
                                text = text.replace('\t', '')
                                row_list.append(text)

                    # image_td = item.find('td', {'class': 'image'})
                    # image = image_td.find('img')

                    # if image:
                        # img_src = image.attrs.get('src')

                        # img_file_name = os.path.basename(img_src)

                        # img_bin = self.session.get((SITE_URL + img_src), stream=True)

                        # img_name_path = './images/' + img_file_name

                        # row_list.append(img_name_path)

                        # with open(img_name_path, 'wb') as out_file:
                            # shutil.copyfileobj(img_bin.raw, out_file)
                        # del img_bin

                    for column_text in row_list:
                        new_cell = self.ws.cell(row=row, column=column)
                        new_cell.border = border
                        new_cell.value = column_text
                        column += 1

                    column = 1
                    row += 1

            except Exception as e:
                print('-------- EXCEPTION', e)

        self.wb.save('result.xlsx')

if __name__ == '__main__':

    parser = Parser()
    parser.start_parse()